﻿Group: Discount requirements
FriendlyName: Give discount based on customer's cart value
SystemName: DiscountRequirement.CartValue
Version: 1.0
SupportedVersions: 3.90
Author: Wherrelz IT Solutions Pvt Ltd
DisplayOrder: 1
FileName: Nop.Plugin.DiscountRules.CartValue.dll
Description: This plugin allows you to set a discount based on the current value of the shopping cart