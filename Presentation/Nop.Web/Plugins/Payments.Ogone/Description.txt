﻿Group: Payment methods
FriendlyName: Payment Ogone
SystemName: Payments.Ogone
Version: 1.90
SupportedVersions: 3.90
Author: Matrid team
DisplayOrder: 1
FileName: Nop.Plugin.Payments.Ogone.dll
Description: This plugin enables oneline payment using Ogone payment method