IF EXISTS (
		SELECT *
		FROM sys.objects
		WHERE object_id = OBJECT_ID(N'[dbo].[nop4you_AbandonedCart]') AND OBJECTPROPERTY(object_id,N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[nop4you_AbandonedCart]
GO
CREATE PROCEDURE [dbo].[nop4you_AbandonedCart]
@customerreminderId int
AS
Begin

declare @storeId int
declare @allowRenew bit
declare @fromDate datetime
declare @fromAmount decimal, @toAmount decimal

select top 1 @storeId = StoreId, @allowRenew = AllowRenew, @fromDate = FromDate,
@fromAmount = FromAmount, @toAmount = ToAmount
from [dbo].[n4y_Reminder] where id = @customerreminderId

declare @customers table
(Id int IDENTITY(1,1),
 CustomerId int,
 LastUpdateCartDateUtc datetime,
 LevelId int)

if((select top 1 '*' from [dbo].[n4y_ReminderCondition] where [CustomerReminderId] = @customerreminderId) = '*')
Begin
	insert @customers(CustomerId, LastUpdateCartDateUtc)
	select T0.CustomerId, max(T0.UpdatedOnUtc) from [dbo].[ShoppingCartItem] T0
	inner join [dbo].[n4y_ReminderCondition] T1 on T1.[CustomerReminderId] = @customerreminderId and T1.Value = convert(nvarchar(10), T0.ProductId)
	inner join Customer T2 on T2.Id = T0.CustomerId and isnull(T2.Email, '')!=''
	where T0.ShoppingCartTypeId = 1 and (T0.StoreId = @storeId or @storeId = 0)
	and (@fromDate is null or T0.UpdatedOnUtc > @fromDate)
	group by T0.CustomerId
End
else
Begin
	insert @customers(CustomerId, LastUpdateCartDateUtc)
	select T0.CustomerId, max(T0.UpdatedOnUtc) from [dbo].[ShoppingCartItem] T0
	inner join Customer T1 on T1.Id = T0.CustomerId and isnull(T1.Email, '')!=''
	where T0.ShoppingCartTypeId = 1 and (T0.StoreId = @storeId or @storeId = 0)
	and (@fromDate is null or T0.UpdatedOnUtc > @fromDate)
	group by T0.CustomerId
End

declare @datettimeutcnow datetime
set @datettimeutcnow = GETUTCDATE()

declare @RowNum int
set @RowNum = 0

while(1=1)
Begin

declare @customerId int
declare @lastupdatedatecustomer datetime
set @customerId = (select top 1 customerId from @customers where Id>@RowNum order by Id)
if(@customerId is NULL)
break;

set @RowNum= (select top 1 Id from @customers where Id>@RowNum order by Id)
set @lastupdatedatecustomer = (select top 1 LastUpdateCartDateUtc from @customers where customerId = @customerId order by Id)

if(@fromAmount+@toAmount > 0)
Begin
	declare @sumAmount decimal 
	set @sumAmount = 0

	select @sumAmount = sum(T0.Quantity*T1.Price) from [dbo].[ShoppingCartItem] T0
	inner join Product T1 on T1.Id = T0.ProductId
	where T0.CustomerId = @customerId 
	and T0.ShoppingCartTypeId = 1 and (T0.StoreId = @storeId or @storeId = 0)
	and (@fromDate is null or T0.UpdatedOnUtc > @fromDate)

	if((@sumAmount > @toAmount and @toAmount != 0) or (@fromAmount > @sumAmount and @fromAmount != 0))
	Begin
		delete from @customers where Customerid = @customerId
	End
End

if((select top 1 '*' from [dbo].[n4y_ReminderHistory] where [CustomerReminderId] = @customerreminderId and CustomerId = @customerId) = '*')
Begin
	declare @maxhistoryid int
	declare @maxhistoryCustomerReminderLevelId int
	declare @maxhistoryLevel int
	declare @nextCustomerReminderLevelId int
	declare @maxHistoryDate datetime
	declare @lastsenddate datetime

	select @maxhistoryid = max(Id), @maxHistoryDate = max(CreateDateUtc) from [dbo].[n4y_ReminderHistory] where [CustomerReminderId] = @customerreminderId and CustomerId = @customerId
	select @maxhistoryCustomerReminderLevelId = CustomerReminderLevelId, @maxhistoryLevel = [Level], @lastsenddate = CreateDateUtc from [dbo].[n4y_ReminderHistory] where Id = @maxhistoryid
	
	select @nextCustomerReminderLevelId = Id from [dbo].[n4y_ReminderLevel] where CustomerReminderId = @customerreminderId and [Level] > @maxhistoryLevel
	if(@nextCustomerReminderLevelId is not null)
	Begin
		declare @nday int, @nhours int, @nminutes int
		select top 1 @nday = [Day], @nhours = [Hour], @nminutes = [Minutes]  from [n4y_ReminderLevel] where Id = @nextCustomerReminderLevelId
		set @lastsenddate = DATEADD(d,@nday,@lastsenddate)
		set @lastsenddate = DATEADD(hh,@nhours,@lastsenddate)
		set @lastsenddate = DATEADD(mi,@nminutes,@lastsenddate)

		if(@datettimeutcnow > @lastsenddate)
		Begin
			update @customers set LevelId = @nextCustomerReminderLevelId where customerId = @customerId
		End
		else
		Begin
			delete from @customers where Customerid = @customerId
		End
		
	End
	Else
	Begin
		if(@allowRenew=0)
		begin
			delete from @customers where Customerid = @customerId
		End
		Else
		Begin
			if(@maxHistoryDate > @lastupdatedatecustomer)
			begin
				delete from @customers where Customerid = @customerId
			End
			else
			Begin
				update @customers set LevelId = (select top 1 Id from [n4y_ReminderLevel] where CustomerReminderId = @customerreminderId order by [Level] asc) 
				where customerId = @customerId
			End
		End
	End
End
else
Begin
	declare @levelId int
	set @levelId = isnull((select top 1 Id from [dbo].[n4y_ReminderLevel] where CustomerReminderId = @customerreminderId order by [Level]),-1)
	if(@levelId=-1)
	begin
		delete from @customers
	End
	Else
	begin
		declare @day int, @hours int, @minutes int

		select top 1 @day = [Day], @hours = [Hour], @minutes = [Minutes] from [n4y_ReminderLevel] where Id = @levelId
		set @lastupdatedatecustomer = DATEADD(d,@day,@lastupdatedatecustomer)
		set @lastupdatedatecustomer = DATEADD(hh,@hours,@lastupdatedatecustomer)
		set @lastupdatedatecustomer = DATEADD(mi,@minutes,@lastupdatedatecustomer)

		if(@datettimeutcnow > @lastupdatedatecustomer)
		Begin
			update @customers set LevelId = @levelId where customerId = @customerId
		End
	End
End
End

select CustomerId, LevelId from @customers where isnull(LevelId,0) != 0

End
GO
IF EXISTS (
		SELECT *
		FROM sys.objects
		WHERE object_id = OBJECT_ID(N'[dbo].[nop4you_UnpaidOrder]') AND OBJECTPROPERTY(object_id,N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[nop4you_UnpaidOrder]
GO
CREATE PROCEDURE [dbo].[nop4you_UnpaidOrder]
@customerreminderId int
AS
Begin

declare @storeId int
declare @allowRenew bit
declare @fromDate datetime
declare @addays int
declare @fromAmount decimal, @toAmount decimal

select top 1 @storeId = StoreId, @allowRenew = AllowRenew, @fromDate = FromDate, @addays = [Days],
@fromAmount = FromAmount, @toAmount = ToAmount
from [dbo].[n4y_Reminder] where id = @customerreminderId

declare @datettimeutcnows datetime
set @datettimeutcnows = convert(datetime, convert(nvarchar(10), GETUTCDATE(),120))


declare @customers table
(Id int IDENTITY(1,1),
 CustomerId int,
 OrderId int,
 CreateOrderDateUtc datetime,
 LevelId int)

declare @datettimeutcnow datetime
set @datettimeutcnow = GETUTCDATE()


if((select top 1 '*' from [dbo].[n4y_ReminderCondition] where [CustomerReminderId] = @customerreminderId) = '*')
Begin

insert @customers(CustomerId, OrderId, CreateOrderDateUtc)
select distinct T0.Id, T5.Id, T5.CreatedOnUtc from [dbo].[Customer] T0
inner join [Order] T5 on T5.CustomerId = T0.Id and T5.PaymentStatusId = 10 and (T5.StoreId = @storeId or @storeId = 0)
inner join [OrderItem] T6 on T6.OrderId = T5.Id
inner join [dbo].[n4y_ReminderCondition] T7 on T7.[CustomerReminderId] = @customerreminderId and T7.Value = convert(nvarchar(10), T6.ProductId)
and (T5.OrderTotal <= @toAmount or @toAmount = 0)
and (T5.OrderTotal >= @fromAmount or @fromAmount = 0)
where (@fromDate is null or T5.CreatedOnUtc > @fromDate)
and DateAdd(dd,@addays, T5.CreatedOnUtc) < @datettimeutcnow
End
else
Begin
insert @customers(CustomerId, OrderId, CreateOrderDateUtc)
select distinct T0.Id, T5.Id, T5.CreatedOnUtc from [dbo].[Customer] T0
inner join [dbo].GenericAttribute T1 on T1.EntityId = T0.Id and T1.KeyGroup = 'Customer'
inner join [Order] T5 on T5.CustomerId = T0.Id and T5.PaymentStatusId = 10 and (T5.StoreId = @storeId or @storeId = 0)
and (T5.OrderTotal <= @toAmount or @toAmount = 0)
and (T5.OrderTotal >= @fromAmount or @fromAmount = 0)
where (@fromDate is null or T5.CreatedOnUtc > @fromDate)
and DateAdd(dd,@addays, T5.CreatedOnUtc) < @datettimeutcnow
end

declare @RowNum int
set @RowNum = 0

while(1=1)
Begin

declare @customerId int
declare @OrderId int
declare @CreateOrderDateUtc datetime
set @customerId = (select top 1 customerId from @customers where Id>@RowNum order by Id)
if(@customerId is NULL)
break;

set @RowNum= (select top 1 Id from @customers where Id>@RowNum order by Id)
set @CreateOrderDateUtc = (select top 1 CreateOrderDateUtc from @customers where Id = @RowNum order by Id)
set @OrderId = (select top 1 OrderId from @customers where Id = @RowNum order by Id)
set @customerId = (select top 1 customerId from @customers where Id = @RowNum order by Id)

if((select top 1 '*' from [dbo].[n4y_ReminderHistory] where [CustomerReminderId] = @customerreminderId and OrderId = @OrderId and CustomerId = @customerId) = '*')
Begin
	declare @maxhistoryid int
	declare @maxhistoryCustomerReminderLevelId int
	declare @maxhistoryLevel int
	declare @nextCustomerReminderLevelId int
	declare @maxHistoryDate datetime
	declare @lastsenddate datetime

	select @maxhistoryid = max(Id), @maxHistoryDate = max(CreateDateUtc) from [dbo].[n4y_ReminderHistory] 
		where [CustomerReminderId] = @customerreminderId and CustomerId = @customerId and OrderId = @OrderId
	select @maxhistoryCustomerReminderLevelId = CustomerReminderLevelId, @maxhistoryLevel = [Level], @lastsenddate = CreateDateUtc 
		from [dbo].[n4y_ReminderHistory] where Id = @maxhistoryid
	
	select @nextCustomerReminderLevelId = Id from [dbo].[n4y_ReminderLevel] where CustomerReminderId = @customerreminderId and [Level] > @maxhistoryLevel
	if(@nextCustomerReminderLevelId is not null)
	Begin
		declare @nday int, @nhours int, @nminutes int
		select top 1 @nday = [Day], @nhours = [Hour], @nminutes = [Minutes]  from [n4y_ReminderLevel] where Id = @nextCustomerReminderLevelId
		set @lastsenddate = DATEADD(d,@nday,@lastsenddate)
		set @lastsenddate = DATEADD(hh,@nhours,@lastsenddate)
		set @lastsenddate = DATEADD(mi,@nminutes,@lastsenddate)
		
		if(@datettimeutcnow > @lastsenddate)
		Begin
			update @customers set LevelId = @nextCustomerReminderLevelId where customerId = @customerId and OrderId = @OrderId
		End
		else
		Begin
			delete from @customers where Customerid = @customerId and OrderId = @OrderId
		End
		
	End
	Else
	Begin
		if(@allowRenew=0)
		begin
			delete from @customers where Customerid = @customerId and OrderId = @OrderId
		End
		Else
		Begin			
			if(year(@maxHistoryDate) = year(@datettimeutcnow))
			begin
				delete from @customers where Customerid = @customerId and OrderId = @OrderId
			End
			else
			Begin
				update @customers set LevelId = (select top 1 Id from [n4y_ReminderLevel] where CustomerReminderId = @customerreminderId order by [Level] asc) 
				where customerId = @customerId and OrderId = @OrderId
			End
		End
	End
End
else
Begin
	declare @levelId int
	set @levelId = isnull((select top 1 Id from [dbo].[n4y_ReminderLevel] where CustomerReminderId = @customerreminderId order by [Level]),-1)
	if(@levelId=-1)
	begin
		delete from @customers
	End
	Else
	begin
		declare @day int, @hours int, @minutes int

		select top 1 @day = [Day], @hours = [Hour], @minutes = [Minutes] from [n4y_ReminderLevel] where Id = @levelId
		set @CreateOrderDateUtc = DATEADD(d,@day,@CreateOrderDateUtc)
		set @CreateOrderDateUtc = DATEADD(hh,@hours,@CreateOrderDateUtc)
		set @CreateOrderDateUtc = DATEADD(mi,@minutes,@CreateOrderDateUtc)

		if(@datettimeutcnow > @CreateOrderDateUtc)
		Begin
			update @customers set LevelId = @levelId where customerId = @customerId and OrderId = @OrderId
		End
	End
End
End

select CustomerId, OrderId, LevelId from @customers where isnull(LevelId,0) != 0

End
GO
IF EXISTS (
		SELECT *
		FROM sys.objects
		WHERE object_id = OBJECT_ID(N'[dbo].[nop4you_CompletedOrder]') AND OBJECTPROPERTY(object_id,N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[nop4you_CompletedOrder]
GO
CREATE PROCEDURE [dbo].[nop4you_CompletedOrder]
@customerreminderId int
AS
Begin

declare @storeId int
declare @allowRenew bit
declare @fromDate datetime
declare @addays int
declare @fromAmount decimal, @toAmount decimal

select top 1 @storeId = StoreId, @allowRenew = AllowRenew, @fromDate = FromDate, @addays = [Days],
@fromAmount = FromAmount, @toAmount = ToAmount
from [dbo].[n4y_Reminder] where id = @customerreminderId

declare @datettimeutcnows datetime
set @datettimeutcnows = convert(datetime, convert(nvarchar(10), GETUTCDATE(),120))


declare @customers table
(Id int IDENTITY(1,1),
 CustomerId int,
 OrderId int,
 CreateOrderDateUtc datetime,
 LevelId int)

declare @datettimeutcnow datetime
set @datettimeutcnow = GETUTCDATE()

if((select top 1 '*' from [dbo].[n4y_ReminderCondition] where [CustomerReminderId] = @customerreminderId) = '*')
Begin

insert @customers(CustomerId, OrderId, CreateOrderDateUtc)
select distinct T0.Id, T5.Id, T5.CreatedOnUtc from [dbo].[Customer] T0
inner join [Order] T5 on T5.CustomerId = T0.Id and T5.OrderStatusId = 30 and (T5.StoreId = @storeId or @storeId = 0)
inner join [OrderItem] T6 on T6.OrderId = T5.Id
inner join [dbo].[n4y_ReminderCondition] T7 on T7.[CustomerReminderId] = @customerreminderId and T7.Value = convert(nvarchar(10), T6.ProductId)
and (T5.OrderTotal <= @toAmount or @toAmount = 0)
and (T5.OrderTotal >= @fromAmount or @fromAmount = 0)
where (@fromDate is null or T5.CreatedOnUtc > @fromDate)
and DateAdd(dd,@addays, T5.CreatedOnUtc) < @datettimeutcnow

End
Else
Begin


insert @customers(CustomerId, OrderId, CreateOrderDateUtc)
select distinct T0.Id, T5.Id, T5.CreatedOnUtc from [dbo].[Customer] T0
inner join [dbo].GenericAttribute T1 on T1.EntityId = T0.Id and T1.KeyGroup = 'Customer'
inner join [Order] T5 on T5.CustomerId = T0.Id and T5.OrderStatusId = 30 and (T5.StoreId = @storeId or @storeId = 0)
and (T5.OrderTotal <= @toAmount or @toAmount = 0)
and (T5.OrderTotal >= @fromAmount or @fromAmount = 0)
where (@fromDate is null or T5.CreatedOnUtc > @fromDate)
and DateAdd(dd,@addays, T5.CreatedOnUtc) < @datettimeutcnow

End

declare @RowNum int
set @RowNum = 0

while(1=1)
Begin

declare @customerId int
declare @OrderId int
declare @CreateOrderDateUtc datetime
set @customerId = (select top 1 customerId from @customers where Id>@RowNum order by Id)
if(@customerId is NULL)
break;

set @RowNum= (select top 1 Id from @customers where Id>@RowNum order by Id)
set @CreateOrderDateUtc = (select top 1 CreateOrderDateUtc from @customers where Id = @RowNum order by Id)
set @OrderId = (select top 1 OrderId from @customers where Id = @RowNum order by Id)
set @customerId = (select top 1 customerId from @customers where Id = @RowNum order by Id)

if((select top 1 '*' from [dbo].[n4y_ReminderHistory] where [CustomerReminderId] = @customerreminderId and OrderId = @OrderId and CustomerId = @customerId) = '*')
Begin
	declare @maxhistoryid int
	declare @maxhistoryCustomerReminderLevelId int
	declare @maxhistoryLevel int
	declare @nextCustomerReminderLevelId int
	declare @maxHistoryDate datetime
	declare @lastsenddate datetime

	select @maxhistoryid = max(Id), @maxHistoryDate = max(CreateDateUtc) from [dbo].[n4y_ReminderHistory] 
		where [CustomerReminderId] = @customerreminderId and CustomerId = @customerId and OrderId = @OrderId
	select @maxhistoryCustomerReminderLevelId = CustomerReminderLevelId, @maxhistoryLevel = [Level], @lastsenddate = CreateDateUtc 
		from [dbo].[n4y_ReminderHistory] where Id = @maxhistoryid
	
	select @nextCustomerReminderLevelId = Id from [dbo].[n4y_ReminderLevel] where CustomerReminderId = @customerreminderId and [Level] > @maxhistoryLevel
	if(@nextCustomerReminderLevelId is not null)
	Begin
		declare @nday int, @nhours int, @nminutes int
		select top 1 @nday = [Day], @nhours = [Hour], @nminutes = [Minutes]  from [n4y_ReminderLevel] where Id = @nextCustomerReminderLevelId
		set @lastsenddate = DATEADD(d,@nday,@lastsenddate)
		set @lastsenddate = DATEADD(hh,@nhours,@lastsenddate)
		set @lastsenddate = DATEADD(mi,@nminutes,@lastsenddate)
		
		if(@datettimeutcnow > @lastsenddate)
		Begin
			update @customers set LevelId = @nextCustomerReminderLevelId where customerId = @customerId and OrderId = @OrderId
		End
		else
		Begin
			delete from @customers where Customerid = @customerId and OrderId = @OrderId
		End
		
	End
	Else
	Begin
		if(@allowRenew=0)
		begin
			delete from @customers where Customerid = @customerId and OrderId = @OrderId
		End
		Else
		Begin			
			if(year(@maxHistoryDate) = year(@datettimeutcnow))
			begin
				delete from @customers where Customerid = @customerId and OrderId = @OrderId
			End
			else
			Begin
				update @customers set LevelId = (select top 1 Id from [n4y_ReminderLevel] where CustomerReminderId = @customerreminderId order by [Level] asc) 
				where customerId = @customerId and OrderId = @OrderId
			End
		End
	End
End
else
Begin
	declare @levelId int
	set @levelId = isnull((select top 1 Id from [dbo].[n4y_ReminderLevel] where CustomerReminderId = @customerreminderId order by [Level]),-1)
	if(@levelId=-1)
	begin
		delete from @customers
	End
	Else
	begin
		declare @day int, @hours int, @minutes int

		select top 1 @day = [Day], @hours = [Hour], @minutes = [Minutes] from [n4y_ReminderLevel] where Id = @levelId
		set @CreateOrderDateUtc = DATEADD(d,@day,@CreateOrderDateUtc)
		set @CreateOrderDateUtc = DATEADD(hh,@hours,@CreateOrderDateUtc)
		set @CreateOrderDateUtc = DATEADD(mi,@minutes,@CreateOrderDateUtc)

		if(@datettimeutcnow > @CreateOrderDateUtc)
		Begin
			update @customers set LevelId = @levelId where customerId = @customerId and OrderId = @OrderId
		End
	End
End
End

select CustomerId, OrderId, LevelId from @customers where isnull(LevelId,0) != 0

End
GO
IF EXISTS (
		SELECT *
		FROM sys.objects
		WHERE object_id = OBJECT_ID(N'[dbo].[nop4you_RegisteredCustomer]') AND OBJECTPROPERTY(object_id,N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[nop4you_RegisteredCustomer]
GO
CREATE PROCEDURE [dbo].[nop4you_RegisteredCustomer]
@customerreminderId int
AS
Begin

declare @storeId int
declare @allowRenew bit
declare @fromDate datetime
select top 1 @storeId = StoreId, @allowRenew = AllowRenew, @fromDate = FromDate 
from [dbo].[n4y_Reminder] where id = @customerreminderId

declare @customers table
(Id int IDENTITY(1,1),
 CustomerId int,
 CreateDateUtc datetime,
 LevelId int)

if((select top 1 '*' from [dbo].[n4y_ReminderCondition] where [CustomerReminderId] = @customerreminderId) = '*')
Begin

	insert @customers(CustomerId, CreateDateUtc)
	select distinct T0.Id, T0.CreatedOnUtc from [dbo].[Customer] T0
	inner join [dbo].GenericAttribute T1 on T1.EntityId = T0.Id and T1.KeyGroup = 'Customer'
	inner join [dbo].[n4y_ReminderCondition] T2 on T2.CustomerReminderId = @customerreminderId and T2.[Key] = T1.[Key] and T2.Value = T1.Value
	inner join (select distinct EntityId, KeyGroup, StoreId from [dbo].GenericAttribute) T3 on T3.EntityId = T0.Id and T1.KeyGroup = 'Customer' and (T3.StoreId = @storeId or @storeId = 0)
	where isnull(T0.Email,'')!=''
	and (@fromDate is null or T0.CreatedOnUtc > @fromDate)

End
else
Begin
	insert @customers(CustomerId, CreateDateUtc)
	select distinct T0.Id, T0.CreatedOnUtc from [dbo].[Customer] T0
	inner join GenericAttribute T1 on T1.EntityId = T0.Id and T1.KeyGroup = 'Customer'
	inner join (select distinct EntityId, KeyGroup, StoreId from [dbo].GenericAttribute) T3 on T3.EntityId = T0.Id and T1.KeyGroup = 'Customer' and (T3.StoreId = @storeId or @storeId = 0)
	where isnull(T0.Email,'')!=''
	and (@fromDate is null or T0.CreatedOnUtc > @fromDate)
End

declare @datettimeutcnow datetime
set @datettimeutcnow = GETUTCDATE()

declare @RowNum int
set @RowNum = 0

while(1=1)
Begin

declare @customerId int
declare @createdatecustomer datetime
set @customerId = (select top 1 customerId from @customers where Id>@RowNum order by Id)
if(@customerId is NULL)
break;

set @RowNum= (select top 1 Id from @customers where Id>@RowNum order by Id)
set @createdatecustomer = (select top 1 CreateDateUtc from @customers where customerId = @customerId order by Id)


if((select top 1 '*' from [dbo].[n4y_ReminderHistory] where [CustomerReminderId] = @customerreminderId and CustomerId = @customerId) = '*')
Begin
	declare @maxhistoryid int
	declare @maxhistoryCustomerReminderLevelId int
	declare @maxhistoryLevel int
	declare @nextCustomerReminderLevelId int
	declare @maxHistoryDate datetime
	declare @lastsenddate datetime

	select @maxhistoryid = max(Id), @maxHistoryDate = max(CreateDateUtc) from [dbo].[n4y_ReminderHistory] where [CustomerReminderId] = @customerreminderId and CustomerId = @customerId
	select @maxhistoryCustomerReminderLevelId = CustomerReminderLevelId, @maxhistoryLevel = [Level], @lastsenddate = CreateDateUtc from [dbo].[n4y_ReminderHistory] where Id = @maxhistoryid
	
	select @nextCustomerReminderLevelId = Id from [dbo].[n4y_ReminderLevel] where CustomerReminderId = @customerreminderId and [Level] > @maxhistoryLevel
	if(@nextCustomerReminderLevelId is not null)
	Begin
		declare @nday int, @nhours int, @nminutes int
		select top 1 @nday = [Day], @nhours = [Hour], @nminutes = [Minutes]  from [n4y_ReminderLevel] where Id = @nextCustomerReminderLevelId
		set @lastsenddate = DATEADD(d,@nday,@lastsenddate)
		set @lastsenddate = DATEADD(hh,@nhours,@lastsenddate)
		set @lastsenddate = DATEADD(mi,@nminutes,@lastsenddate)

		if(@datettimeutcnow > @lastsenddate)
		Begin
			update @customers set LevelId = @nextCustomerReminderLevelId where customerId = @customerId
		End
		else
		Begin
			delete from @customers where Customerid = @customerId
		End
		
	End
	Else
	Begin
		if(@allowRenew=0)
		begin
			delete from @customers where Customerid = @customerId
		End
		Else
		Begin
			if(@maxHistoryDate > @createdatecustomer)
			begin
				delete from @customers where Customerid = @customerId
			End
			else
			Begin
				update @customers set LevelId = (select top 1 Id from [n4y_ReminderLevel] where CustomerReminderId = @customerreminderId order by [Level] asc) 
				where customerId = @customerId
			End
		End
	End
End
else
Begin
	declare @levelId int
	set @levelId = isnull((select top 1 Id from [dbo].[n4y_ReminderLevel] where CustomerReminderId = @customerreminderId order by [Level]),-1)
	if(@levelId=-1)
	begin
		delete from @customers
	End
	Else
	begin
		declare @day int, @hours int, @minutes int

		select top 1 @day = [Day], @hours = [Hour], @minutes = [Minutes] from [n4y_ReminderLevel] where Id = @levelId
		set @createdatecustomer = DATEADD(d,@day,@createdatecustomer)
		set @createdatecustomer = DATEADD(hh,@hours,@createdatecustomer)
		set @createdatecustomer = DATEADD(mi,@minutes,@createdatecustomer)

		if(@datettimeutcnow > @createdatecustomer)
		Begin
			update @customers set LevelId = @levelId where customerId = @customerId
		End
	End
End
End

select CustomerId, LevelId from @customers where isnull(LevelId,0) != 0

End
GO
IF EXISTS (
		SELECT *
		FROM sys.objects
		WHERE object_id = OBJECT_ID(N'[dbo].[nop4you_LastPurchase]') AND OBJECTPROPERTY(object_id,N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[nop4you_LastPurchase]
GO
CREATE PROCEDURE [dbo].[nop4you_LastPurchase]
@customerreminderId int
AS
Begin

declare @storeId int
declare @allowRenew bit
declare @fromDate datetime
select top 1 @storeId = StoreId, @allowRenew = AllowRenew, @fromDate = FromDate 
from [dbo].[n4y_Reminder] where id = @customerreminderId

declare @customers table
(Id int IDENTITY(1,1),
 CustomerId int,
 LastPurchaseDateUtc datetime,
 LevelId int)

if((select top 1 '*' from [dbo].[n4y_ReminderCondition] where [CustomerReminderId] = @customerreminderId) = '*')
Begin

	insert @customers(CustomerId, LastPurchaseDateUtc)
	select T0.Id, max(T4.CreatedOnUtc) from [dbo].[Customer] T0
	inner join [dbo].GenericAttribute T1 on T1.EntityId = T0.Id and T1.KeyGroup = 'Customer'
	inner join [dbo].[n4y_ReminderCondition] T2 on T2.CustomerReminderId = @customerreminderId and T2.[Key] = T1.[Key] and T2.Value = T1.Value
	inner join (select distinct EntityId, KeyGroup, StoreId from [dbo].GenericAttribute) T3 on T3.EntityId = T0.Id and T1.KeyGroup = 'Customer' and (T3.StoreId = @storeId or @storeId = 0)
	inner join [Order] T4 on T4.CustomerId = T0.Id
	where isnull(T0.Email,'')!=''	
	group by T0.Id
	having (@fromDate is null or max(T4.CreatedOnUtc) > @fromDate)
End
else
Begin
	insert @customers(CustomerId, LastPurchaseDateUtc)
	select T0.Id, max(T4.CreatedOnUtc) from [dbo].[Customer] T0
	inner join GenericAttribute T1 on T1.EntityId = T0.Id and T1.KeyGroup = 'Customer'
	inner join (select distinct EntityId, KeyGroup, StoreId from [dbo].GenericAttribute) T3 on T3.EntityId = T0.Id and T1.KeyGroup = 'Customer' and (T3.StoreId = @storeId or @storeId = 0)
	inner join [Order] T4 on T4.CustomerId = T0.Id
	where isnull(T0.Email,'')!=''
	group by T0.Id
	having (@fromDate is null or max(T4.CreatedOnUtc) > @fromDate)

End

declare @datettimeutcnow datetime
set @datettimeutcnow = GETUTCDATE()

declare @RowNum int
set @RowNum = 0

while(1=1)
Begin

declare @customerId int
declare @LastPurchaseDateUtcCustomer datetime
set @customerId = (select top 1 customerId from @customers where Id>@RowNum order by Id)
if(@customerId is NULL)
break;

set @RowNum= (select top 1 Id from @customers where Id>@RowNum order by Id)
set @LastPurchaseDateUtcCustomer = (select top 1 LastPurchaseDateUtc from @customers where customerId = @customerId order by Id)


if((select top 1 '*' from [dbo].[n4y_ReminderHistory] where [CustomerReminderId] = @customerreminderId and CustomerId = @customerId) = '*')
Begin
	declare @maxhistoryid int
	declare @maxhistoryCustomerReminderLevelId int
	declare @maxhistoryLevel int
	declare @nextCustomerReminderLevelId int
	declare @maxHistoryDate datetime
	declare @lastsenddate datetime

	select @maxhistoryid = max(Id), @maxHistoryDate = max(CreateDateUtc) from [dbo].[n4y_ReminderHistory] where [CustomerReminderId] = @customerreminderId and CustomerId = @customerId
	select @maxhistoryCustomerReminderLevelId = CustomerReminderLevelId, @maxhistoryLevel = [Level], @lastsenddate = CreateDateUtc from [dbo].[n4y_ReminderHistory] where Id = @maxhistoryid
	
	select @nextCustomerReminderLevelId = Id from [dbo].[n4y_ReminderLevel] where CustomerReminderId = @customerreminderId and [Level] > @maxhistoryLevel
	if(@nextCustomerReminderLevelId is not null)
	Begin
		declare @nday int, @nhours int, @nminutes int
		select top 1 @nday = [Day], @nhours = [Hour], @nminutes = [Minutes]  from [n4y_ReminderLevel] where Id = @nextCustomerReminderLevelId
		set @lastsenddate = DATEADD(d,@nday,@lastsenddate)
		set @lastsenddate = DATEADD(hh,@nhours,@lastsenddate)
		set @lastsenddate = DATEADD(mi,@nminutes,@lastsenddate)

		if(@datettimeutcnow > @lastsenddate)
		Begin
			update @customers set LevelId = @nextCustomerReminderLevelId where customerId = @customerId
		End
		else
		Begin
			delete from @customers where Customerid = @customerId
		End
		
	End
	Else
	Begin
		if(@allowRenew=0)
		begin
			delete from @customers where Customerid = @customerId
		End
		Else
		Begin
			if(@maxHistoryDate > @LastPurchaseDateUtcCustomer)
			begin
				delete from @customers where Customerid = @customerId
			End
			else
			Begin
				update @customers set LevelId = (select top 1 Id from [n4y_ReminderLevel] where CustomerReminderId = @customerreminderId order by [Level] asc) 
				where customerId = @customerId
			End
		End
	End
End
else
Begin
	declare @levelId int
	set @levelId = isnull((select top 1 Id from [dbo].[n4y_ReminderLevel] where CustomerReminderId = @customerreminderId order by [Level]),-1)
	if(@levelId=-1)
	begin
		delete from @customers
	End
	Else
	begin
		declare @day int, @hours int, @minutes int

		select top 1 @day = [Day], @hours = [Hour], @minutes = [Minutes] from [n4y_ReminderLevel] where Id = @levelId
		set @LastPurchaseDateUtcCustomer = DATEADD(d,@day,@LastPurchaseDateUtcCustomer)
		set @LastPurchaseDateUtcCustomer = DATEADD(hh,@hours,@LastPurchaseDateUtcCustomer)
		set @LastPurchaseDateUtcCustomer = DATEADD(mi,@minutes,@LastPurchaseDateUtcCustomer)

		if(@datettimeutcnow > @LastPurchaseDateUtcCustomer)
		Begin
			update @customers set LevelId = @levelId where customerId = @customerId
		End
	End
End
End

select CustomerId, LevelId from @customers where isnull(LevelId,0) != 0

End
GO
IF EXISTS (
		SELECT *
		FROM sys.objects
		WHERE object_id = OBJECT_ID(N'[dbo].[nop4you_Birthday]') AND OBJECTPROPERTY(object_id,N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[nop4you_Birthday]
GO
CREATE PROCEDURE [dbo].[nop4you_Birthday]
@customerreminderId int
AS
Begin

declare @storeId int
declare @allowRenew bit
declare @fromDate datetime
declare @addays int

select top 1 @storeId = StoreId, @allowRenew = AllowRenew, @fromDate = FromDate, @addays = [Days]
from [dbo].[n4y_Reminder] where id = @customerreminderId

declare @datettimeutcnows datetime
set @datettimeutcnows = convert(datetime, convert(nvarchar(10), GETUTCDATE(),120))


declare @customers table
(Id int IDENTITY(1,1),
 CustomerId int,
 BirthdayDateUtc datetime,
 LevelId int)

if((select top 1 '*' from [dbo].[n4y_ReminderCondition] where [CustomerReminderId] = @customerreminderId) = '*')
Begin

	insert @customers(CustomerId, BirthdayDateUtc)
	select T0.Id, convert(datetime,max(T4.Value)) from [dbo].[Customer] T0
	inner join [dbo].GenericAttribute T1 on T1.EntityId = T0.Id and T1.KeyGroup = 'Customer'
	inner join [dbo].[n4y_ReminderCondition] T2 on T2.CustomerReminderId = @customerreminderId and T2.[Key] = T1.[Key] and T2.Value = T1.Value
	inner join (select distinct EntityId, KeyGroup, StoreId from [dbo].GenericAttribute) T3 on T3.EntityId = T0.Id and T1.KeyGroup = 'Customer' and (T3.StoreId = @storeId or @storeId = 0)
	inner join GenericAttribute T4 on T4.EntityId = T0.Id and T4.KeyGroup = 'Customer' and T4.[Key] = 'DateOfBirth' and isnull(T4.Value, '')!=''
	where isnull(T0.Email,'')!=''	
	group by T0.Id
	having (@fromDate is null or convert(datetime,max(T4.Value)) > @fromDate)
	and
		@datettimeutcnows
		between 
		dateadd(dd, -@addays, convert(datetime, 
		convert(nvarchar(4), year(@datettimeutcnows))+'-'+ 
		convert(nvarchar(2), month(convert(datetime,max(T4.Value))))+'-'+
		convert(nvarchar(2), day(convert(datetime,max(T4.Value)))))
		) 
		and 
		convert(datetime, 
		convert(nvarchar(4), year(@datettimeutcnows))+'-'+ 
		convert(nvarchar(2), month(convert(datetime,max(T4.Value))))+'-'+
		convert(nvarchar(2), day(convert(datetime,max(T4.Value)))))

End
else
Begin
	insert @customers(CustomerId, BirthdayDateUtc)
	select T0.Id, convert(datetime,max(T4.Value)) from [dbo].[Customer] T0
	inner join GenericAttribute T1 on T1.EntityId = T0.Id and T1.KeyGroup = 'Customer'
	inner join (select distinct EntityId, KeyGroup, StoreId from [dbo].GenericAttribute) T3 on T3.EntityId = T0.Id and T1.KeyGroup = 'Customer' and (T3.StoreId = @storeId or @storeId = 0)
	inner join GenericAttribute T4 on T4.EntityId = T0.Id and T4.KeyGroup = 'Customer' and T4.[Key] = 'DateOfBirth' and isnull(T4.Value, '')!=''
	where isnull(T0.Email,'')!=''
	group by T0.Id
	having (@fromDate is null or convert(datetime,max(T4.Value)) > @fromDate)
	and
		@datettimeutcnows
		between 
		dateadd(dd, -@addays, convert(datetime, 
		convert(nvarchar(4), year(@datettimeutcnows))+'-'+ 
		convert(nvarchar(2), month(convert(datetime,max(T4.Value))))+'-'+
		convert(nvarchar(2), day(convert(datetime,max(T4.Value)))))
		) 
		and 
		convert(datetime, 
		convert(nvarchar(4), year(@datettimeutcnows))+'-'+ 
		convert(nvarchar(2), month(convert(datetime,max(T4.Value))))+'-'+
		convert(nvarchar(2), day(convert(datetime,max(T4.Value)))))

End

declare @datettimeutcnow datetime
set @datettimeutcnow = GETUTCDATE()

declare @RowNum int
set @RowNum = 0

while(1=1)
Begin

declare @customerId int
declare @BirthDateUtcCustomer datetime
set @customerId = (select top 1 customerId from @customers where Id>@RowNum order by Id)
if(@customerId is NULL)
break;

set @RowNum= (select top 1 Id from @customers where Id>@RowNum order by Id)
set @BirthDateUtcCustomer = (select top 1 BirthdayDateUtc from @customers where customerId = @customerId order by Id)


if((select top 1 '*' from [dbo].[n4y_ReminderHistory] where [CustomerReminderId] = @customerreminderId and CustomerId = @customerId) = '*')
Begin
	declare @maxhistoryid int
	declare @maxhistoryCustomerReminderLevelId int
	declare @maxhistoryLevel int
	declare @nextCustomerReminderLevelId int
	declare @maxHistoryDate datetime
	declare @lastsenddate datetime

	select @maxhistoryid = max(Id), @maxHistoryDate = max(CreateDateUtc) from [dbo].[n4y_ReminderHistory] where [CustomerReminderId] = @customerreminderId and CustomerId = @customerId
	select @maxhistoryCustomerReminderLevelId = CustomerReminderLevelId, @maxhistoryLevel = [Level], @lastsenddate = CreateDateUtc from [dbo].[n4y_ReminderHistory] where Id = @maxhistoryid
	
	select @nextCustomerReminderLevelId = Id from [dbo].[n4y_ReminderLevel] where CustomerReminderId = @customerreminderId and [Level] > @maxhistoryLevel
	if(@nextCustomerReminderLevelId is not null)
	Begin
		declare @nday int, @nhours int, @nminutes int
		select top 1 @nday = [Day], @nhours = [Hour], @nminutes = [Minutes]  from [n4y_ReminderLevel] where Id = @nextCustomerReminderLevelId
		set @lastsenddate = DATEADD(d,@nday,@lastsenddate)
		set @lastsenddate = DATEADD(hh,@nhours,@lastsenddate)
		set @lastsenddate = DATEADD(mi,@nminutes,@lastsenddate)
		
		if(@datettimeutcnow > @lastsenddate)
		Begin
			update @customers set LevelId = @nextCustomerReminderLevelId where customerId = @customerId
		End
		else
		Begin
			delete from @customers where Customerid = @customerId
		End
		
	End
	Else
	Begin
		if(@allowRenew=0)
		begin
			delete from @customers where Customerid = @customerId
		End
		Else
		Begin			
			if(year(@maxHistoryDate) = year(@datettimeutcnow))
			begin
				delete from @customers where Customerid = @customerId
			End
			else
			Begin
				update @customers set LevelId = (select top 1 Id from [n4y_ReminderLevel] where CustomerReminderId = @customerreminderId order by [Level] asc) 
				where customerId = @customerId
			End
		End
	End
End
else
Begin
	declare @levelId int
	set @levelId = isnull((select top 1 Id from [dbo].[n4y_ReminderLevel] where CustomerReminderId = @customerreminderId order by [Level]),-1)
	if(@levelId=-1)
	begin
		delete from @customers
	End
	Else
	begin
		declare @day int, @hours int, @minutes int

		select top 1 @day = [Day], @hours = [Hour], @minutes = [Minutes] from [n4y_ReminderLevel] where Id = @levelId
		set @BirthDateUtcCustomer = DATEADD(d,@day,@BirthDateUtcCustomer)
		set @BirthDateUtcCustomer = DATEADD(hh,@hours,@BirthDateUtcCustomer)
		set @BirthDateUtcCustomer = DATEADD(mi,@minutes,@BirthDateUtcCustomer)

		if(@datettimeutcnow > @BirthDateUtcCustomer)
		Begin
			update @customers set LevelId = @levelId where customerId = @customerId
		End
	End
End
End

select CustomerId, LevelId from @customers where isnull(LevelId,0) != 0

End
GO