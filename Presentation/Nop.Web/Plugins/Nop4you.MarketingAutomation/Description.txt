Group: nop4you
FriendlyName: Marketing automation
SystemName: Nop4you.MarketingAutomation
Version: 1.03
SupportedVersions: 3.90
Author: nop4you
DisplayOrder: 1
FileName: Nop4you.Plugin.MarketingAutomation.dll
Description: Send emails which your customers want receive. Show them products which they want view. Help them with the buying process.
LimitedToStores: 2