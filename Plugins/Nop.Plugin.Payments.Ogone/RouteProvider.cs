﻿using System.Web.Mvc;
using System.Web.Routing;
using Nop.Web.Framework.Mvc.Routes;

namespace Nop.Plugin.Payments.Ogone
{
    public partial class RouteProvider : IRouteProvider
    {
        public void RegisterRoutes(RouteCollection routes)
        {
            RouteCollectionExtensions.MapRoute(routes, "Plugin.Payments.Ogone.Configure",
                  "Configure",
                  new { controller = "PaymentOgone", action = "Configure" },
                  new[] { "Nop.Plugin.Payments.Ogone.Controllers" }
            );

            RouteCollectionExtensions.MapRoute(routes, "Plugin.Payments.Ogone.PaymentInfo",
                    "PaymentInfo",
                    new { controller = "PaymentOgone", action = "PaymentInfo" },
                    new string[1] { "Nop.Plugin.Payments.Ogone.Controllers" }
                );

            RouteCollectionExtensions.MapRoute(routes, "Plugin.Payments.Ogone.AcceptPayment",
                "AcceptPayment",
                new { controller = "PaymentOgone", action = "AcceptPayment" },
                new string[1] { "Nop.Plugin.Payments.Ogone.Controllers" }
              );

            RouteCollectionExtensions.MapRoute(routes, "Plugin.Payments.Ogone.OgoneResult",
                    "OgoneResult",
                    new { controller = "PaymentOgone", action = "OgoneResult" },
                    new string[1] { "Nop.Plugin.Payments.Ogone.Controllers" }
                );

            RouteCollectionExtensions.MapRoute(routes, "Plugin.Payments.Ogone.OgoneCancel",
                  "OgoneCancel",
                  new { controller = "PaymentOgone", action = "OgoneCancel" },
                  new string[1] { "Nop.Plugin.Payments.Ogone.Controllers" }
              );


            RouteCollectionExtensions.MapRoute(routes, "Plugin.Payments.Ogone.PostBackHandler", "Plugins/PaymentOgone/PostBackHandler", (object)new
            {
                controller = "PaymentOgone",
                action = "PostBackHandler"
            },
            new string[1] { "Nop.Plugin.Payments.Ogone.Controllers" });


        }
        public int Priority
        {
            get
            {
                return 0;
            }
        }
    }
}
