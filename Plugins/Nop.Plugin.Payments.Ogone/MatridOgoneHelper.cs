﻿using Nop.Core.Domain.Payments;

namespace Nop.Plugin.Payments.Ogone
{
    public class MatridOgoneHelper
    {
        private const string Status_Authorized = "5";
        private const string Status_Payment_Requested = "9";
        private const string Status_Authorization_Waiting = "51";
        private const string Status_Payment_Processing = "91";
        private const string Status_CacelledByClient = "1";
        private const string Status_CancelledByMerchant = "6";
        private const string Status_Authorization_Refused = "2";
        private const string Status_Payment_Refused = "93";
        private const string Status_InvalidOr_Incomplete = "0";
        private const string Status_Authorization_Notknown = "52";
        private const string Status_Payment_Uncertain = "92";


        public static PaymentStatus GetPaymentStatus(string status, string error)
        {
            switch (status)
            {
                case "5":
                    return (PaymentStatus)20;
                case "9":
                    return (PaymentStatus)30;
                case "93":
                case "0":
                case "51":
                case "91":
                case "52":
                case "92":
                case "2":
                    return (PaymentStatus)10;
                case "1":
                case "6":
                    return (PaymentStatus)50;
                default:
                    return (PaymentStatus)10;
            }
        }
    }
}
