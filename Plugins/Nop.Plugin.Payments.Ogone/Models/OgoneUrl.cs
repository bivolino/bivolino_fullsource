﻿using System.ComponentModel;
using System.Web.Mvc;
using Nop.Web.Framework.Mvc;
using Nop.Web.Framework;

namespace Nop.Plugin.Payments.Ogone
{
    public class OgoneUrl:BaseNopModel
    {
        public string Url { get; set; }
        public string orderId { get; set; }
        public string storeurl { get; set; }
    }
}