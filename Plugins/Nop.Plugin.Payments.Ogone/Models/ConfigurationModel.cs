﻿using System.ComponentModel;
using System.Web.Mvc;
using Nop.Web.Framework.Mvc;
using Nop.Web.Framework;

namespace Nop.Plugin.Payments.Ogone
{
	public class ConfigurationModel : BaseNopModel
	{
        public int ActiveStoreScopeConfiguration { get; set; }
		[NopResourceDisplayName("Plugins.Payment.Ogone.PSPID")]
		public string PSPID { get; set; }

		[NopResourceDisplayName("Plugins.Payment.Ogone.AdditionalFee")]
		public decimal AdditionalFee { get; set; }

		[NopResourceDisplayName("Plugins.Payment.Ogone.Currency")]
		public string Currency { get; set; }

		[NopResourceDisplayName("Plugins.Payment.Ogone.Language")]
		public string Language { get; set; }

		[NopResourceDisplayName("Plugins.Payment.Ogone.AcceptUrl")]
		public string AcceptUrl { get; set; }

		[NopResourceDisplayName("Plugins.Payment.Ogone.DeclineUrl")]
		public string DeclineUrl { get; set; }

		[NopResourceDisplayName("Plugins.Payment.Ogone.ExceptionUrl")]
		public string ExceptionUrl { get; set; }

		[NopResourceDisplayName("Plugins.Payment.Ogone.CancelUrl")]
		public string CancelUrl { get; set; }

		///// Miscellanous

		[NopResourceDisplayName("Plugins.Payment.Ogone.HomeUrl")]
		public string HomeUrl { get; set; }

		[NopResourceDisplayName("Plugins.Payment.Ogone.CatalogUrl")]
		public string CatalogUrl { get; set; }

		[NopResourceDisplayName("Plugins.Payment.Ogone.CN")]
		public string CN { get; set; }

		[NopResourceDisplayName("Plugins.Payment.Ogone.OwnerZIP")]
		public string OwnerZIP { get; set; }

		[NopResourceDisplayName("Plugins.Payment.Ogone.OwnerAddress")]
		public string OwnerAddress { get; set; }

		[NopResourceDisplayName("Plugins.Payment.Ogone.SHASign")]
		public string SHASign { get; set; }

		[NopResourceDisplayName("Plugins.Payment.Ogone.Alias")]
		public string Alias { get; set; }

		[NopResourceDisplayName("Plugins.Payment.Ogone.AliasUsage")]
		public string AliasUsage { get; set; }

		[NopResourceDisplayName("Plugins.Payment.Ogone.AliasOperation")]
		public string AliasOperation { get; set; }

		[NopResourceDisplayName("Plugins.Payment.Ogone.COM")]
		public string COM { get; set; }

		[NopResourceDisplayName("Plugins.Payment.Ogone.COMPLUS")]
		public string COMPLUS { get; set; }

        [DisplayName("Ogone ID Prefix")]
        public string OrderIdPrefix { get; set; }

	}
}
