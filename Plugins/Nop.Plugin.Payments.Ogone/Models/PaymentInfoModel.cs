﻿using System.Collections.Generic;
using System.Web.Mvc;
using Nop.Web.Framework;
using Nop.Web.Framework.Mvc;

namespace Nop.Plugin.Payments.Ogone.Models
{
	public class PaymentInfoModel : BaseNopModel
	{
		public string pspId { get; set; }
		public string orderid { get; set; }
		public string curr { get; set; }
		public string amount { get; set; }
		public string lang { get; set; }
		public string shaSign { get; set; }
		public string urlredirectlayout { get; set; }
		public string HTMLHiddenField { get; set; }
		public string AcceptUrl { get; set; }
		public string brand { get; set; }


	}
}
