﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Nop.Core.Configuration;

namespace Nop.Plugin.Payments.Ogone
{
	public class OgonePaymentSettings : ISettings
	{
		public string PSPID { get; set; }
		public decimal AdditionalFee { get; set; }
		public string Currency { get; set; }
		public string Language { get; set; }
		public string AcceptUrl { get; set; }
		public string DeclineUrl { get; set; }
		public string ExceptionUrl { get; set; }
		public string CancelUrl { get; set; }

		///// Miscellanous

		public string HomeUrl { get; set; }
		public string CatalogUrl { get; set; }
		public string CN { get; set; }
		public string OwnerZIP { get; set; }
		public string OwnerAddress { get; set; }
		public string SHASign { get; set; }
		public string Alias { get; set; }
		public string AliasUsage { get; set; }
		public string AliasOperation { get; set; }
		public string COM { get; set; }
		public string COMPLUS { get; set; }

		public bool AdditionalFeePercentage { get; set; }
        public string OrderIdPrefix { get; set; }

	}
}
