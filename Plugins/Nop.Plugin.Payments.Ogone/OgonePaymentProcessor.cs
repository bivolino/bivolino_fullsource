﻿using System;
using System.IO;
using System.Linq;
using System.Text;
using Nop.Core;
using Nop.Core.Domain;
using Nop.Core.Domain.Directory;
//using Nop.Core.Domain.Localization;
using Nop.Core.Domain.Orders;
using Nop.Core.Domain.Payments;
using Nop.Core.Plugins;
using Nop.Plugin.Payments.Ogone.Components;
using Nop.Plugin.Payments.Ogone.Controllers;
using Nop.Services.Catalog;
using Nop.Services.Configuration;
using Nop.Services.Directory;
using Nop.Services.Localization;
using Nop.Services.Orders;
using Nop.Services.Payments;
using Nop.Services.Security;
using Nop.Services.Tax;
using System.Collections.Generic;
using System.Globalization;
using System.Security.Cryptography;
using System.Threading.Tasks;
using System.Web.Routing;



namespace Nop.Plugin.Payments.Ogone
{
    public class OgonePaymentProcessor : BasePlugin, IPaymentMethod, IPlugin
    {
        #region Fields
        private readonly OgonePaymentSettings _ogonePaymentSettings;
        private readonly ISettingService _settingService;
        private readonly ITaxService _taxService;
        private readonly IPriceCalculationService _priceCalculationService;
        private readonly ICurrencyService _currencyService;
        private readonly CurrencySettings _currencySettings;
        private readonly IWebHelper _webHelper;
        private readonly StoreInformationSettings _storeInformationSettings;
        private readonly IOrderTotalCalculationService _orderTotalCalculationService;
        private readonly IEncryptionService _encryptionService;
        private readonly IWorkContext _workContext;
        private readonly IStoreContext _storecontext;
        private readonly IOrderService _orderService;
        private readonly ILocalizationService _localizationService;
        #endregion

        #region Ctor

        public OgonePaymentProcessor(OgonePaymentSettings ogonePaymentSettings,
             ISettingService settingService,
             ITaxService taxService, IPriceCalculationService priceCalculationService,
             ICurrencyService currencyService,
             CurrencySettings currencySettings, IWebHelper webHelper,
             StoreInformationSettings storeInformationSettings, IOrderTotalCalculationService orderTotalCalculationService,
            IEncryptionService encryptionService, IWorkContext workContext, IStoreContext storeContext, IOrderService orderService,
            ILocalizationService localization)
        {
            this._ogonePaymentSettings = ogonePaymentSettings;
            this._settingService = settingService;
            this._taxService = taxService;
            this._priceCalculationService = priceCalculationService;
            this._currencyService = currencyService;
            this._currencySettings = currencySettings;
            this._webHelper = webHelper;
            this._storeInformationSettings = storeInformationSettings;
            this._orderTotalCalculationService = orderTotalCalculationService;
            this._encryptionService = encryptionService;
            this._workContext = workContext;
            this._storecontext = storeContext;
            this._orderService = orderService;
            this._localizationService = localization;
        }

        #endregion

        #region method
        /// <summary>
        /// Process a payment
        /// </summary>
        /// <param name="processPaymentRequest">Payment info required for an order processing</param>
        /// <returns>Process payment result</returns>
        public ProcessPaymentResult ProcessPayment(ProcessPaymentRequest processPaymentRequest)
        {
            var result = new ProcessPaymentResult();
            //result.NewPaymentStatus = PaymentStatus.Pending;
            result.NewPaymentStatus = PaymentStatus.Paid;
            return result;
        }

        /// <summary>
        /// Post process payment (used by payment gateways that require redirecting to a third-party URL)
        /// </summary>
        /// <param name="postProcessPaymentRequest">Payment info required for an order processing</param>
        //public void PostProcessPayment(PostProcessPaymentRequest postProcessPaymentRequest)
        //{
        //nothing
        //}

        public void PostProcessPayment(PostProcessPaymentRequest postProcessPaymentRequest)
        {
            var result = new ProcessPaymentRequest();
            



            return;
            
            Order order = postProcessPaymentRequest.Order;
            PostData PostData1 = new PostData("OgoneForm", this._ogonePaymentSettings.AcceptUrl);
            decimal num;
            string str1;
            if (!string.IsNullOrEmpty(this._ogonePaymentSettings.OrderIdPrefix))
            {
                str1 = string.Format("{0}{1}", (object)this._ogonePaymentSettings.OrderIdPrefix, (object)((BaseEntity)order).Id);
            }
            else
            {
                num = ((BaseEntity)order).Id;
                str1 = num.ToString();
            }
            string str2 = str1;
            PostData1.Add("PSPID", this._ogonePaymentSettings.PSPID);
            PostData1.Add("orderID", str2);
            PostData PostData2 = PostData1;
            string name = "amount";
            var ordertotal = Convert.ToDecimal(postProcessPaymentRequest.Order.OrderTotal);
            num = ordertotal * new decimal(100);

            string str3 = num.ToString((IFormatProvider)CultureInfo.InvariantCulture);
            PostData2.Add(name, str3);
            string currencyCode = this._currencyService.GetCurrencyById(this._currencySettings.PrimaryStoreCurrencyId).CurrencyCode;
            PostData1.Add("currency", currencyCode);
            string str4 = string.Format("{0} {1}", postProcessPaymentRequest.Order.BillingAddress.FirstName, postProcessPaymentRequest.Order.BillingAddress.LastName);
            PostData1.Add("CN", str4);
            string str5 = string.Format("{0} {1}", postProcessPaymentRequest.Order.BillingAddress.Address1, postProcessPaymentRequest.Order.BillingAddress.Address2);
            PostData1.Add("owneraddress", str5);
            PostData1.Add("EMAIL", postProcessPaymentRequest.Order.Customer.Email);
            PostData1.Add("ownerZIP", postProcessPaymentRequest.Order.BillingAddress.ZipPostalCode);
            PostData1.Add("language", this._workContext.WorkingLanguage.LanguageCulture);
            //PostData1.Add("TP", this._ogonePaymentSettings.TemplateUrl);
            //PostData1.Add("TITLE", this._ogonePaymentSettings.TemplateTitle);
            //PostData1.Add("BGCOLOR", this._ogonePaymentSettings.BackgroundColor);
            //PostData1.Add("TXTCOLOR", this._ogonePaymentSettings.TextColor);
            //PostData1.Add("TBLBGCOLOR", this._ogonePaymentSettings.TableBackgroundColor);
            //PostData1.Add("TBLTXTCOLOR", this._ogonePaymentSettings.TableTextColor);
            //PostData1.Add("BUTTONBGCOLOR", this._ogonePaymentSettings.ButtonBackgroundColor);
            //PostData1.Add("BUTTONTXTCOLOR", this._ogonePaymentSettings.ButtonTextColor);
            //PostData1.Add("FONTTYPE", this._ogonePaymentSettings.FontFamily);
            //PostData1.Add("LOGO", this._ogonePaymentSettings.LogoUrl);
            string url = this._storecontext.CurrentStore.Url;
            if (!url.EndsWith("/"))
                url += "/";
            PostData1.Add("accepturl", url + "Plugins/PaymentOgone/AcceptPayment");
            PostData1.Add("cancelurl", url + "Plugins/PaymentOgone/CancelPayment");
            //PostData1.Add("declineurl", url + "Plugins/PaymentOgone/DeclinePayment");
            // PostData1.Add("exceptionurl", url + "Plugins/PaymentOgone/ExceptionPayment");
            // if (!string.IsNullOrEmpty(this._ogonePaymentSettings.ParamVar))
            //     PostData1.Add("PARAMVAR", this._ogonePaymentSettings.ParamVar);
            //  if (!string.IsNullOrEmpty(this._ogonePaymentSettings.PmList))
            //     PostData1.Add("PMLIST", this._ogonePaymentSettings.PmList);
            // if (!string.IsNullOrEmpty(this._ogonePaymentSettings.ExclPmList))
            //     PostData1.Add("EXCLPMLIST", this._ogonePaymentSettings.ExclPmList);
            string hashPassPhrase = this._encryptionService.DecryptText(this._ogonePaymentSettings.SHASign, "");
            string hashText1 = this.GetHashText(PostData1.SortedPostFields, hashPassPhrase);
            string hashText2 = this.GetHashText(PostData1.SortedPostFields, "+++HASHKEY+++");
            string sha = this.CalculateSHA(hashText1);
            PostData1.Add("SHASIGN", sha);
            this.AddOrderNote(order, string.Format("Hashed Text:{0}\r\nSHA Digest:{1}", (object)hashText2, (object)sha), false);
            PostData1.Post();
        }

        /// <summary>
        /// Gets additional handling fee
        /// </summary>
        /// <returns>Additional handling fee</returns>
        //public decimal GetAdditionalHandlingFee()
        //{
        //	return _ogonePaymentSettings.AdditionalFee;
        //}

        public decimal GetAdditionalHandlingFee(IList<ShoppingCartItem> cart)
        {
            var result = this.CalculateAdditionalFee(_orderTotalCalculationService, cart,
                 _ogonePaymentSettings.AdditionalFee, _ogonePaymentSettings.AdditionalFeePercentage);
            return result;
        }

        /// <summary>
        /// Returns a value indicating whether payment method should be hidden during checkout
        /// </summary>
        /// <param name="cart">Shoping cart</param>
        /// <returns>true - hide; false - display.</returns>
        public bool HidePaymentMethod(IList<ShoppingCartItem> cart)
        {
            //you can put any logic here
            //for example, hide this payment method if all products in the cart are downloadable
            //or hide this payment method if current customer is from certain country
            return false;
        }

        /// <summary>
        /// Captures payment
        /// </summary>
        /// <param name="capturePaymentRequest">Capture payment request</param>
        /// <returns>Capture payment result</returns>
        public CapturePaymentResult Capture(CapturePaymentRequest capturePaymentRequest)
        {
            var result = new CapturePaymentResult();
            result.AddError("Capture method not supported");
            return result;
        }

        /// <summary>
        /// Refunds a payment
        /// </summary>
        /// <param name="refundPaymentRequest">Request</param>
        /// <returns>Result</returns>
        public RefundPaymentResult Refund(RefundPaymentRequest refundPaymentRequest)
        {
            var result = new RefundPaymentResult();
            result.AddError("Refund method not supported");
            return result;
        }

        /// <summary>
        /// Voids a payment
        /// </summary>
        /// <param name="voidPaymentRequest">Request</param>
        /// <returns>Result</returns>
        public VoidPaymentResult Void(VoidPaymentRequest voidPaymentRequest)
        {
            var result = new VoidPaymentResult();
            result.AddError("Void method not supported");
            return result;
        }

        /// <summary>
        /// Process recurring payment
        /// </summary>
        /// <param name="processPaymentRequest">Payment info required for an order processing</param>
        /// <returns>Process payment result</returns>
        public ProcessPaymentResult ProcessRecurringPayment(ProcessPaymentRequest processPaymentRequest)
        {
            var result = new ProcessPaymentResult();
            result.AddError("Recurring payment not supported");
            return result;
        }

        /// <summary>
        /// Cancels a recurring payment
        /// </summary>
        /// <param name="cancelPaymentRequest">Request</param>
        /// <returns>Result</returns>
        public CancelRecurringPaymentResult CancelRecurringPayment(CancelRecurringPaymentRequest cancelPaymentRequest)
        {
            var result = new CancelRecurringPaymentResult();
            result.AddError("Recurring payment not supported");
            return result;
        }

        /// <summary>
        /// Gets a value indicating whether customers can complete a payment after order is placed but not completed (for redirection payment methods)
        /// </summary>
        /// <param name="order">Order</param>
        /// <returns>Result</returns>
        public bool CanRePostProcessPayment(Order order)
        {
            if (order == null)
                throw new ArgumentNullException("order");

            //it's not a redirection payment method. So we always return false
            return false;
        }

        /// <summary>
        /// Gets a route for provider configuration
        /// </summary>
        /// <param name="actionName">Action name</param>
        /// <param name="controllerName">Controller name</param>
        /// <param name="routeValues">Route values</param>
        public void GetConfigurationRoute(out string actionName, out string controllerName, out System.Web.Routing.RouteValueDictionary routeValues)
        {
            actionName = "Configure";
            controllerName = "PaymentOgone";
            routeValues = new RouteValueDictionary() { { "Namespaces", "Nop.Plugin.Payments.Ogone.Controllers" }, { "area", null } };

        }

        /// <summary>
        /// Gets a route for payment info
        /// </summary>
        /// <param name="actionName">Action name</param>
        /// <param name="controllerName">Controller name</param>
        /// <param name="routeValues">Route values</param>
        public void GetPaymentInfoRoute(out string actionName, out string controllerName, out RouteValueDictionary routeValues)
        {
            actionName = "PaymentInfo";
            controllerName = "PaymentOgone";
            routeValues = new RouteValueDictionary() { { "Namespaces", "Nop.Plugin.Payments.Ogone.Controllers" }, { "area", null } };
        }

        public Type GetControllerType()
        {
            return typeof(PaymentOgoneController);
        }

        public override void Install()
        {
            this.AddOrUpdatePluginLocaleResource("Plugins.Payments.Ogone.PaymentMethodDescription", "Pay by credit / debit card");

            this.AddOrUpdatePluginLocaleResource("Plugins.Payment.Ogone.PSPID", "PSPID: ");
            this.AddOrUpdatePluginLocaleResource("Plugins.Payment.Ogone.AdditionalFee", "AdditionalFee: ");
            this.AddOrUpdatePluginLocaleResource("Plugins.Payment.Ogone.AcceptUrl", "Accept Url: ");
            this.AddOrUpdatePluginLocaleResource("Plugins.Payment.Ogone.CancelUrl", "Cancel Url: ");
            this.AddOrUpdatePluginLocaleResource("Plugins.Payment.Ogone.CatalogUrl", "Catalog Url: ");
            this.AddOrUpdatePluginLocaleResource("Plugins.Payment.Ogone.DeclineUrl", "Decline Url: ");
            this.AddOrUpdatePluginLocaleResource("Plugins.Payment.Ogone.ExceptionUrl", "Exception Url: ");
            this.AddOrUpdatePluginLocaleResource("Plugins.Payment.Ogone.HomeUrl", "Home Url: ");
            this.AddOrUpdatePluginLocaleResource("Plugins.Payment.Ogone.Alias", "Alias: ");
            this.AddOrUpdatePluginLocaleResource("Plugins.Payment.Ogone.AliasOperation", "Alias Operation: ");
            this.AddOrUpdatePluginLocaleResource("Plugins.Payment.Ogone.AliasUsage", "Alias Usage: ");
            this.AddOrUpdatePluginLocaleResource("Plugins.Payment.Ogone.CN", "CN: ");
            this.AddOrUpdatePluginLocaleResource("Plugins.Payment.Ogone.COM", "COM: ");
            this.AddOrUpdatePluginLocaleResource("Plugins.Payment.Ogone.COMPLUS", "COMPLUS: ");
            this.AddOrUpdatePluginLocaleResource("Plugins.Payment.Ogone.Currency", "Currency: ");
            this.AddOrUpdatePluginLocaleResource("Plugins.Payment.Ogone.Language", "Language: ");
            this.AddOrUpdatePluginLocaleResource("Plugins.Payment.Ogone.OwnerAddress", "Owner Address: ");
            this.AddOrUpdatePluginLocaleResource("Plugins.Payment.Ogone.SHASign", "SHASign: ");
            base.Install();
        }



        public override void Uninstall()
        {
            this.DeletePluginLocaleResource("Plugins.Payments.Ogone.PaymentMethodDescription");
            this.DeletePluginLocaleResource("Plugins.Payment.Ogone.PSPID");
            this.DeletePluginLocaleResource("Plugins.Payment.Ogone.AdditionalFee");
            this.DeletePluginLocaleResource("Plugins.Payment.Ogone.AcceptUrl");
            this.DeletePluginLocaleResource("Plugins.Payment.Ogone.CancelUrl");
            this.DeletePluginLocaleResource("Plugins.Payment.Ogone.CatalogUrl");
            this.DeletePluginLocaleResource("Plugins.Payment.Ogone.DeclineUrl");
            this.DeletePluginLocaleResource("Plugins.Payment.Ogone.ExceptionUrl");
            this.DeletePluginLocaleResource("Plugins.Payment.Ogone.HomeUrl");
            this.DeletePluginLocaleResource("Plugins.Payment.Ogone.Alias");
            this.DeletePluginLocaleResource("Plugins.Payment.Ogone.AliasOperation");
            this.DeletePluginLocaleResource("Plugins.Payment.Ogone.AliasUsage");
            this.DeletePluginLocaleResource("Plugins.Payment.Ogone.CN");
            this.DeletePluginLocaleResource("Plugins.Payment.Ogone.COM");
            this.DeletePluginLocaleResource("Plugins.Payment.Ogone.COMPLUS");
            this.DeletePluginLocaleResource("Plugins.Payment.Ogone.Currency");
            this.DeletePluginLocaleResource("Plugins.Payment.Ogone.Language");
            this.DeletePluginLocaleResource("Plugins.Payment.Ogone.OwnerAddress");
            this.DeletePluginLocaleResource("Plugins.Payment.Ogone.SHASign");
            base.Uninstall();           
        }



        public bool VerifyHashDigest(SortedDictionary<string, string> values, string digest)
        {
            return this.CalculateSHA(this.GetHashText(values, this._encryptionService.DecryptText(this._ogonePaymentSettings.SHASign, ""))).Equals(digest);
        }

        private string CalculateSHA(string hashText)
        {
            return BitConverter.ToString(new SHA1CryptoServiceProvider().ComputeHash(new UTF8Encoding().GetBytes(hashText))).Replace("-", string.Empty);
        }

        private string GetHashText(SortedDictionary<string, string> formFields, string hashPassPhrase)
        {
            StringBuilder stringBuilder = new StringBuilder();
            foreach (string key in formFields.Keys)
            {
                if (!string.IsNullOrEmpty(formFields[key]))
                    stringBuilder.AppendFormat("{0}={1}{2}", (object)key.ToUpper(), (object)formFields[key], (object)hashPassPhrase);
            }
            return stringBuilder.ToString();
        }

        private void AddOrderNote(Order order, string noteText, bool displayToCustomer)
        {
            ICollection<OrderNote> orderNotes = order.OrderNotes;
            OrderNote orderNote1 = new OrderNote();
            orderNote1.Note = noteText;
            orderNote1.DisplayToCustomer = displayToCustomer;
            orderNote1.CreatedOnUtc = DateTime.UtcNow;
            OrderNote orderNote2 = orderNote1;
            orderNotes.Add(orderNote2);
            this._orderService.UpdateOrder(order);
        }

        #endregion

        #region Properies

        /// <summary>
        /// Gets a value indicating whether capture is supported
        /// </summary>
        public bool SupportCapture
        {
            get
            {
                return false;
            }
        }

        /// <summary>
        /// Gets a value indicating whether partial refund is supported
        /// </summary>
        public bool SupportPartiallyRefund
        {
            get
            {
                return false;
            }
        }

        /// <summary>
        /// Gets a value indicating whether refund is supported
        /// </summary>
        public bool SupportRefund
        {
            get
            {
                return false;
            }
        }

        /// <summary>
        /// Gets a value indicating whether void is supported
        /// </summary>
        public bool SupportVoid
        {
            get
            {
                return false;
            }
        }

        /// <summary>
        /// Gets a recurring payment type of payment method
        /// </summary>
        public RecurringPaymentType RecurringPaymentType
        {
            get
            {
                return RecurringPaymentType.NotSupported;
            }
        }

        /// <summary>
        /// Gets a payment method type
        /// </summary>
        public PaymentMethodType PaymentMethodType
        {
            get
            {
                return PaymentMethodType.Standard;
            }
        }

      

        /// <summary>
        /// Gets a value indicating whether we should display a payment information page for this plugin
        /// </summary>
        public bool SkipPaymentInfo
        {
            get
            {
                return false;
            }
        }

        public string PaymentMethodDescription
        {
            //return description of this payment method to be display on "payment method" checkout step. good practice is to make it localizable
            //for example, for a redirection payment method, description may be like this: "You will be redirected to PayPal site to complete the payment"
            get { return _localizationService.GetResource("Plugins.Payments.Ogone.PaymentMethodDescription"); }
        }
        #endregion

    }
}
